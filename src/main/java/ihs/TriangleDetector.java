package ihs;


import java.math.BigInteger;
import java.util.Arrays;

public class TriangleDetector implements Detector {

    /**
     * @param args
     * @return
     * @throws DetectorException
     */
    public TriangleType run(String... args) throws DetectorException {
        if (args.length != 3) {
            throw new DetectorException(Message.INCORRECT_NUMBER_OF_ARGS.getMessage());
        }

        BigInteger[] sides = new BigInteger[3];

        try {
            for (int i = 0; i < 3; i++) {
                BigInteger side = new BigInteger(args[i]);
                if (side.compareTo(BigInteger.ZERO) < 0) {
                    throw new DetectorException(Message.NOT_NEGATIVE.getMessage());
                }
                sides[i] = side;
            }
        } catch (NumberFormatException e) {
            throw new DetectorException(Message.SIDE_NOT_VALID.getMessage() + e.getMessage());
        }

        Arrays.sort(sides);

        if (BigInteger.ZERO.compareTo(sides[0]) == 0) {
            throw new DetectorException(Message.ZERO_NOT_VALID.getMessage());
        }

        if (sides[2].subtract(sides[1]).compareTo(sides[0]) > 0 || sides[2].subtract(sides[1]).compareTo(sides[0]) == 0) {
            throw new DetectorException(Message.TRIANGLE_NOT_VALID.getMessage());
        }

        if (sides[0].compareTo(sides[2]) == 0) {
            return TriangleType.EQUILATERAL;
        }

        if (sides[0].compareTo(sides[1]) == 0 || sides[1].compareTo(sides[2]) == 0) {
            return TriangleType.ISOSCELES;
        }

        return TriangleType.SCALENE;
    }

}
