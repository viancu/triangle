package ihs;


public enum TriangleType {
    EQUILATERAL("EQUILATERAL"), ISOSCELES("ISOSCELES"), SCALENE("SCALENE");

    String type;

    /**
     *
     * @param type
     */
    TriangleType(String type) {
        this.type = type;
    }

    /**
     *
     * @return
     */
    public String type() {
        return type;
    }
}
