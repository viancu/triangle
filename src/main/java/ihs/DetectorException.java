package ihs;

public class DetectorException extends Exception {
    public DetectorException(String message) {
        super(message);
    }
}
