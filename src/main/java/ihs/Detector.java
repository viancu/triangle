package ihs;


public interface Detector {
    /**
     *
      * @param args
     * @return TriangleType
     * @throws IllegalArgumentException
     */
    TriangleType run(String...args) throws DetectorException;
}
