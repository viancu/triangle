package ihs;

public enum Message {
    INCORRECT_NUMBER_OF_ARGS("Number of triangle sides is not 3. A side should be an integer from 1 to whatever. Example: triangle-1.0.jar 3 4 5"),
    NOT_NEGATIVE("Triangle side should not be negative"),
    SIDE_NOT_VALID("Triangle side is not valid "),
    ZERO_NOT_VALID("Zero is not a valid value"),
    TRIANGLE_NOT_VALID("Triangle sides do not form a valid triangle");

    String message;
    Message(String message) {
        this.message = message;
    }

    String getMessage() {
        return message;
    }
}
