package ihs;


public class App {
    public static void main(String[] args) {
        try {
            TriangleType type = (new TriangleDetector()).run(args);
            System.out.println(type.type());
        } catch (DetectorException e) {
            System.out.println("Error: " + e.getMessage());
        }
    }
}
