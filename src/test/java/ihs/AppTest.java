package ihs;

import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;


public class AppTest {
    OutputStream os;

    @Before
    public void setUp() throws Exception {
        os = new ByteArrayOutputStream();
        System.setOut(new PrintStream(os));
    }

    @Test
    public void shouldAnswerWithScalene() {
        App.main(new String[]{"3", "4", "5"});
        assertThat(os.toString(), containsString(TriangleType.SCALENE.type()));
    }

    @Test
    public void shouldAnswerWithIsosceles() {
        App.main(new String[]{"3", "4", "3"});
        assertThat(os.toString(), containsString(TriangleType.ISOSCELES.type()));
    }

    @Test
    public void shouldAnswerWithEquilateral() {
        App.main(new String[]{"3", "3", "3"});
        assertThat(os.toString(), containsString(TriangleType.EQUILATERAL.type()));
    }

    @Test
    public void shouldAnswerWithErrorZero() {
        App.main(new String[]{"3", "0", "3"});
        assertThat(os.toString(), containsString(Message.ZERO_NOT_VALID.getMessage()));
    }

    @Test
    public void shouldAnswerWithErrorNegative() {
        App.main(new String[]{"3", "-1", "3"});
        assertThat(os.toString(), containsString(Message.NOT_NEGATIVE.getMessage()));
    }

    @Test
    public void shouldAnswerWithErrorTriangle() {
        App.main(new String[]{"3", "4", "12"});
        assertThat(os.toString(), containsString(Message.TRIANGLE_NOT_VALID.getMessage()));
    }

    @Test
    public void shouldAnswerWithErrorSide() {
        App.main(new String[]{"3", "4", "dsadsad"});
        assertThat(os.toString(), containsString(Message.SIDE_NOT_VALID.getMessage()));
    }

    @Test
    public void shouldAnswerWithErrorArgs() {
        App.main(new String[]{"3", "4"});
        assertThat(os.toString(), containsString(Message.INCORRECT_NUMBER_OF_ARGS.getMessage()));
    }

    @Test
    public void shouldCatchException() {
        boolean result = false;
        try {
            (new TriangleDetector()).run();
        } catch (DetectorException e) {
            result = true;
        }

        assertTrue(result);
    }

    @Test
    public void shouldApp() {
        boolean result = false;
        try {
            new App();
            result = true;
        } catch (Exception e) {

        }

        assertTrue(result);
    }
}
