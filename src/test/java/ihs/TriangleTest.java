package ihs;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertEquals;

public class TriangleTest {

    @Rule
    public ExpectedException exception = ExpectedException.none();

    Detector detector = null;

    @Before
    public void setUp() throws Exception {
        detector = new TriangleDetector();
    }

    @Test
    public void argsShouldNotBeEmpty() throws DetectorException {
        exception.expect(DetectorException.class);
        exception.expectMessage(Message.INCORRECT_NUMBER_OF_ARGS.getMessage());
        detector.run();
    }

    @Test
    public void argsShouldNotBeLessThanThree() throws DetectorException {
        exception.expect(DetectorException.class);
        exception.expectMessage(Message.INCORRECT_NUMBER_OF_ARGS.getMessage());
        detector.run(new String[]{"3", "5"});
    }

    @Test
    public void argsShouldNotBeMoreThanThree() throws DetectorException {
        exception.expect(DetectorException.class);
        exception.expectMessage(Message.INCORRECT_NUMBER_OF_ARGS.getMessage());
        detector.run(new String[]{"3", "5", "7", "8"});
    }

    @Test
    public void argsShouldNotBeString() throws DetectorException {
        String testString = "test";
        exception.expect(DetectorException.class);
        exception.expectMessage(Message.SIDE_NOT_VALID.getMessage());
        exception.expectMessage(testString);
        detector.run(new String[]{testString, "3", "4"});
    }

    @Test
    public void argsShouldNotBeNegative() throws DetectorException {
        exception.expect(DetectorException.class);
        exception.expectMessage(Message.NOT_NEGATIVE.getMessage());
        detector.run(new String[]{"5", "-1", "4"});
    }

    @Test
    public void argsShouldNotBeDecimal() throws DetectorException {
        String testString = "10.1";
        exception.expect(DetectorException.class);
        exception.expectMessage(Message.SIDE_NOT_VALID.getMessage());
        exception.expectMessage(testString);
        detector.run(new String[]{"5", "1", testString});
    }

    @Test
    public void argsShouldNotBeZero() throws DetectorException {
        exception.expect(DetectorException.class);
        exception.expectMessage(Message.ZERO_NOT_VALID.getMessage());
        detector.run(new String[]{"5", "0", "4"});
    }

    @Test
    public void triangleShouldNotBeValidIsosceles() throws DetectorException {
        exception.expect(DetectorException.class);
        exception.expectMessage(Message.TRIANGLE_NOT_VALID.getMessage());
        detector.run(new String[]{"15", "3", "3"});
    }


    @Test
    public void triangleShouldNotBeValid() throws DetectorException {
        exception.expect(DetectorException.class);
        exception.expectMessage(Message.TRIANGLE_NOT_VALID.getMessage());
        detector.run(new String[]{"3", "4", "12"});
    }

    @Test
    public void triangleShouldNotBeValidUnsortedList() throws DetectorException {
        exception.expect(DetectorException.class);
        exception.expectMessage(Message.TRIANGLE_NOT_VALID.getMessage());
        detector.run(new String[]{"3", "12", "4"});
    }

    @Test
    public void triangleShouldBeEquilateral() throws DetectorException {
        TriangleType type = detector.run(new String[]{"3", "3", "3"});
        assertEquals(type, TriangleType.EQUILATERAL);
    }

    @Test
    public void triangleShouldBeIsosceles() throws DetectorException {
        TriangleType type = detector.run(new String[]{"3", "2", "3"});
        assertEquals(type, TriangleType.ISOSCELES);
    }


    @Test
    public void triangleShouldNotBeIsosceles() throws DetectorException {
        exception.expect(DetectorException.class);
        exception.expectMessage(Message.TRIANGLE_NOT_VALID.getMessage());
        detector.run(new String[]{"1", "1", "2"});
    }

    @Test
    public void triangleShouldBeScalene() throws DetectorException {
        TriangleType type = detector.run(new String[]{"3", "4", "5"});
        assertEquals(type, TriangleType.SCALENE);
    }

    @Test
    public void triangleShouldBeEquilateralLargeValues() throws DetectorException {
        TriangleType type = detector.run(new String[]{"9223372036854775807", "9223372036854775807", "9223372036854775807"});
        assertEquals(type, TriangleType.EQUILATERAL);
    }

    @Test
    public void triangleShouldNotBeValidWithLargeValues() throws DetectorException {
        exception.expect(DetectorException.class);
        exception.expectMessage(Message.TRIANGLE_NOT_VALID.getMessage());
        detector.run(new String[]{"9223372036854775807", "9223372036854775803", "2"});

    }
}
