package ihs;

import org.junit.Test;
import static org.junit.Assert.*;

public class TriangleTypeTest {

    @Test
    public void shouldBeScalene() {
        assertEquals("SCALENE",TriangleType.SCALENE.type());
    }

    @Test
    public void shouldBeIsosceles() {
        assertEquals("ISOSCELES",TriangleType.ISOSCELES.type());
    }

    @Test
    public void shouldBeEquilateral() {
        assertEquals("EQUILATERAL",TriangleType.EQUILATERAL.type());
    }
}
