package ihs;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MessageTest {
    @Test
    public void shouldBeIncorrect() {
        assertEquals("Number of triangle sides is not 3. A side should be an integer from 1 to whatever. Example: triangle-1.0.jar 3 4 5", Message.INCORRECT_NUMBER_OF_ARGS.getMessage());
    }

    @Test
    public void shouldBeNotNegative() {
        assertEquals("Triangle side should not be negative", Message.NOT_NEGATIVE.getMessage());
    }

    @Test
    public void shouldBeNotValidSide() {
        assertEquals("Triangle side is not valid ", Message.SIDE_NOT_VALID.getMessage());
    }

    @Test
    public void shouldBeNotValidZero() {
        assertEquals("Zero is not a valid value", Message.ZERO_NOT_VALID.getMessage());
    }

    @Test
    public void shouldBeNotValidTriangle() {
        assertEquals("Triangle sides do not form a valid triangle", Message.TRIANGLE_NOT_VALID.getMessage());
    }
}
