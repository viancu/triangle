# README #

Steps necessary to get application up and running.

### What is this repository for? ###

* IHS Markit triangle project
* Version 1.0


### How do I get set up? ###

* git clone git@bitbucket.org:viancu/triangle.git
* cd triangle
* ./mvnw package


### How do I run this program? ###

* java -jar target/triangle-1.0.jar 3 4 5
